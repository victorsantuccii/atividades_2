// Testando a negação - !

namespace operador_nao
{
    let possuiDinheiro = false;
    let estaSemDinheiro = !possuiDinheiro;

    console.log(`Ele realmente está sem dinheiro? ${estaSemDinheiro}` ) //true
}