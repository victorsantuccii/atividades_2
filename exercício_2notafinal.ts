/* Faça um programa que receba as três notas,
cacule e mostre a média ponderada e o conceito que segue a tabela abaixo: */

namespace exercicio_1notafinal
{
    let nota1, nota2, nota3, peso_lab, peso_avs, peso_exf: number;
    nota1 = 10;
    nota2 = 7;
    nota3 = 8;
    peso_lab = 2;
    peso_avs = 3;
    peso_exf = 5;

    let media_p: number;

    media_p = (nota1 * peso_lab + nota2 * peso_avs + nota3 * peso_exf) / (peso_lab + peso_avs + peso_exf)

    if(media_p >=8 && media_p <=10)
    {
        console.log("Termo A");
    }

    if(media_p >=7 && media_p <= 8)
    {
        console.log("Termo B");
    }
    if(media_p >= 6 && media_p <= 7)
    {
        console.log("Termo C");
    }
    if(media_p >= 5 && media_p <= 6)
    {
        console.log("Termo D");

    }
    if(media_p >= 0 && media_p <= 5)
    {
        console.log("Termo E");
    }

}