namespace exemplo_condiçoes
{
    let idade: number = 15;

    if(idade >= 18 ) // if = um 
    {//inicio bloco
        console.log("Pode dirigir");
    }// fim bloco
    else // else = outro ( if - um ou else - outro )
    {
        console.log("Não pode dirigir");
    }
    
    // ternário
    idade >= 18 ? console.log("Pode dirigir") : console.log("Não pode dirigir");

}