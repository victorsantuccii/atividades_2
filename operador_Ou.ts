// Testando o operador OU - ||

namespace operador_Ou
{
    let idade = 18; 
    let maiorIdade = idade >= 18; 
    let possuiAutorizacaoDosPais = false; 
    
    let podeBeber = maiorIdade || possuiAutorizacaoDosPais; 
    
    console.log(`O garoto pode beber? ${podeBeber}`); // true

}