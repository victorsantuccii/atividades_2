/*  Crie um algoritmo que solicite o nome de um usuário e exiba uma mensagem de boas-vindas personalizada
 de acordo com o horário do dia (bom dia, boa tarde ou boa noite).. */

 namespace exercicio_boas_vindas
 {

    const data = new Date();
    const segundos = data.getSeconds();
    const horas = data.getHours();
    const minutos = data.getMinutes();
    const dia = data.getDate();
    const mes = data.getMonth();
    const usuarioN = "Víctor Santucci"

    if( horas >= 6 && horas <= 11 )
    {
        console.log(`Bom dia ${usuarioN}, agora são ${horas} horas, ${minutos} minutos e ${segundos} segundos do dia ${dia}`);
    }
    else if( horas >= 12 && horas <= 18 )
    {
        console.log(`Boa tarde ${usuarioN}, agora são ${horas} horas, ${minutos} minutos e ${segundos} segundos  do dia ${dia} ${mes} de 2023 `);
    }
    else
    {
        console.log(`Boa noite ${usuarioN}, agora são ${horas} horas do dia ${data}`);
    }

 }