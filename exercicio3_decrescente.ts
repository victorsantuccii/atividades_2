/*  Faça um programa que receba três números obrigatoriamente em ordem crescente e um quarto número que não siga esta regra. Mostre, em seguida, os quatro números em ordem decrescente. */
namespace exercicio3_crescente
{
    let n1, n2, n3, nx: number;
    n1 = 2;
    n2 = 3;
    n3 = 4;
    nx = 7;

    if( nx > n3)
    {
        console.log(`${nx}-${n3}-${n2}-${n1}`);
        // console.log(nx + "-" + n3 + "-" + n2 + "-" + n1);
    }
    else if( nx > n2)
    {
        console.log(`${n3} - ${nx} - ${n2} - ${n1}`);

    }
    else if( nx > n1)
    {
        console.log(`${n3} - ${n2} - ${nx} - ${n1}`);
    }
    else
    {
        console.log(`${n3} - ${n2} - ${n1} - ${nx}`);
    }

}